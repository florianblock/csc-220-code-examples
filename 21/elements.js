function ElementManager() {
    /**
     * @type {Visual[]}
     * @private
     */
    this.elements = [];
}

/**
 * 
 * @param {Visual} element
 * @returns {undefined}
 */
ElementManager.prototype.addElement = function(element) {
    this.elements.push(element)
}

ElementManager.prototype.draw = function(g) {
    for (var i = 0; i < this.elements; i++) {
        this.elements[i].draw(g);
    }
}

/**
 * Encapsulates all funcitonality of a visual
 * @constructor
 * @returns {Visual}
 */
function Visual() {
    /**
     * @type {Point}
     * @private
     */
    this.position = new Point();
    
    /**
     * @type {Number}
     * @private
     */
    this.width = 0;
    
    /**
     * @tyupe {Number}
     * @private
     */
    this.height = 0;
}

/**
 * Sets the position of the visual
 * @param {Point} p new position
 * @returns {undefined}
 */
Visual.prototype.setPosition = function(p) {
    this.position = p.clone();
}

/**
 * Sets the width of the visual
 * @param {Number} w
 * @returns {undefined}
 */
Visual.prototype.setWidth = function(w) {
    this.width = w;
}

/**
 * Sets the width of the visual
 * @param {Number} h
 * @returns {undefined}
 */
Visual.prototype.setHeight = function(h) {
    this.width = h;
}

/**
 * Encapsulates a ball
 * @param {Number} radius
 * @constructor
 * @extends Visual
 * @returns {Ball}
 */
function Ball(radius) {
    Visual.call(this);
    this.radius = radius;
    this.vector = new Point();
}

Ball.prototype = new Visual();

/**
 * Sets the vector of the 
 * @param {Point} v Vector
 * @returns {undefined}
 */
Ball.prototype.setVector = function(v) {
    this.vector = v.clone();
}

/**
 * Updates the position of the ball
 * @param {Number} elapsedMilliseconds
 * @returns {undefined}
 */
Ball.prototype.updatePosition = function(elapsedMilliseconds) {
    
}