/**
 * Encapsulates all game logic and elements
 * @constructor
 * @extends GameLoop
 * @returns {Arkanoid}
 */
function Arkanoid() {
    
}

Arkanoid.prototype = new GameLoop();

Arkanoid.prototype.initialize = function(canvas) {
    GameLoop.prototype.initialize.call(this, canvas);
    this.elementManager = new ElementManager();
    this.ball = new Ball(20);
    this.ball.setPosition(new Point(50, 50));
    this.elementManager.addElement(this.ball);
}

Arkanoid.prototype.update = function(elapsedMilliseconds) {
    // your update logic here
}

Arkanoid.prototype.draw = function(g) {
    // your draw logic here
    g.fillStyle = "#CCCCCC";
    g.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.elementManager.draw(g);
}
// </editor-fold>

function initialize() {
    var arkanoid = new Arkanoid();
    arkanoid.initialize(document.getElementById("canvas"));
}

window.onload = initialize;