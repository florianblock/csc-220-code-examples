// <editor-fold desc="ExtendedGameLoop">
function ExtendedGameLoop() {
    
}

ExtendedGameLoop.prototype = new GameLoop();

ExtendedGameLoop.prototype.initialize = function(canvas) {
    GameLoop.prototype.initialize.call(this, canvas);
}

ExtendedGameLoop.prototype.draw = function(g) {
    g.fillStyle = "#CCCCCC";
    g.fillRect(0, 0, this.canvas.width, this.canvas.height);
}
// </editor-fold>

function initialize() {
    var extendedGameLoop = new ExtendedGameLoop();
    extendedGameLoop.initialize(document.getElementById("canvas"));
}

window.onload = initialize;