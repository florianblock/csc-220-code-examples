function Chart(canvas) {
    if (typeof canvas != "undefined") {
        this.canvas = canvas;
        this.initializeGraphics();
        this.initializeInput();
        this.initializeSettings();
    }
}

Chart.prototype.initializeGraphics = function() {
    this.g = this.canvas.getContext("2d");
    this.canvas.width = 600;
    this.canvas.height = 300;
    this.elements = [];
}

Chart.prototype.initializeInput = function() {
    this.canvas.forwardInputTo = this;
    this.canvas.onmousemove = function(e) {
        this.forwardInputTo.onMouseMove(e);
    }
}

Chart.prototype.initializeSettings = function() {
    this.settings = {
        elementWidth : 50,
        maxElementHeight : 300,
        scaleFactor : 2
    }
}

Chart.prototype.onMouseMove = function(e) {
    var position = getRelativeCanvasCoordinates(this.canvas, e);
    for (var i = 0; i < this.elements.length; i++)
    {
        this.elements[i].updateMouseState(position.x, position.y);
    }
    this.draw();
}

Chart.prototype.setData = function(dataSeries) {
    var dataPoints = dataSeries.getData();
    for (var i = 0; i < dataPoints.length; i++) {
        var x = this.settings.elementWidth * i;
        var y = 0;
        var width = this.settings.elementWidth;
        var height = this.settings.maxElementHeight;
        this.elements.push(
                this.initializeChartElement(
                    dataPoints[i], 
                    x, y, width, height,
                    this.settings.scaleFactor)
                );
    }
    this.canvas.width = this.settings.elementWidth * this.elements.length;
    this.canvas.height = this.settings.maxElementHeight;
}

Chart.prototype.initializeChartElement = function(
        dataPoint, x, y, width, height, scaleFactor) {
    // has to return the respective chart element
}

Chart.prototype.draw = function() {
    this.g.fillStyle = "#EEEEEE";
    this.g.fillRect(0, 0, this.canvas.width, this.canvas.height);
    for (var i = 0; i < this.elements.length; i++) {
        this.elements[i].draw(this.g);
    }
}

function ChartElement(dataPoint, x, y, w, h, scaleFactor) {
    this.data = dataPoint;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.scaleFactor = scaleFactor;
    this.isMouseOver = false;
    this.initializeSettings();
}

ChartElement.prototype.initializeSettings = function() {
    // custom settings
    this.settings = {};
    this.settings.labelHeight = 50;
    this.settings.labelFont = "10px Arial"
}

ChartElement.prototype.draw = function(g) {
    g.font = this.settings.labelFont;
    g.fillStyle = "black";
    var labelText = this.data.getLabel();
    var labelWidth = g.measureText(labelText).width;
    g.fillText(labelText, this.x + (this.w - labelWidth) / 2, this.h - 30);
}

ChartElement.prototype.updateMouseState = function(x, y)
{
    if (x < this.x || this.x + this.w < x ||
            y < this.y || this.y + this.h < y)
    {
        this.isMouseOver = false;
    }
    else
    {
        this.isMouseOver = true;
    }
}