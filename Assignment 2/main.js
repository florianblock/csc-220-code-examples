function getRelativeCanvasCoordinates(canvas, e)
{
    var offset = canvas.getBoundingClientRect();
    return { x : e.clientX - offset.left, y : e.clientY - offset.top };
}

function initialize() {
    var testChart = new BarChart(document.getElementById("canvas"));
    testChart.setData(DATA[0]);
    testChart.draw();
    var testChart2 = new BarChart(document.getElementById("canvas2"));
    testChart2.setData(DATA[1]);
    testChart2.draw();
    
}

function onSelectionChanged()
{
//    var chartType = "bar";
//    var dataSeries = DATA[0];
//    var chart = chartType == "bar" ? new BarChart(canvas) : new LineChart...
}

window.onload = initialize;
