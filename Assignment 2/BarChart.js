function BarChart(canvas) {
    Chart.call(this, canvas);
}

BarChart.prototype = new Chart();

BarChart.prototype.initializeChartElement = function(dataPoint, x, y, w, h, 
    scaleFactor) {
    return new BarChartElement(dataPoint, x, y, w, h, scaleFactor);
}

function BarChartElement(dataPoint, x, y, w, h, scaleFactor) {
    ChartElement.call(this, dataPoint, x, y, w, h, scaleFactor);
}

BarChartElement.prototype = new ChartElement();

BarChartElement.prototype.initializeSettings = function() {
    ChartElement.prototype.initializeSettings.call(this);
    this.settings.barPadding = 5;
    this.settings.normalBarColor = "red";
    this.settings.highlightedBarColor = "orange";
}

BarChartElement.prototype.draw = function(g)
{
    ChartElement.prototype.draw.call(this, g);
    g.fillStyle = this.isMouseOver ? this.settings.highlightedBarColor : 
            this.settings.normalBarColor;
    // Does the same than:
//    if (this.isMouseOver) {
//        g.fillStyle = this.settings.highlightedBarColor;
//    } else {
//        g.fillStyle = this.settings.normalBarColor;
//    }
    var barWidth = this.w - 2 * this.settings.barPadding;
    var maxBarHeight = this.h - this.settings.labelHeight;
    var actualBarHeight = this.data.getValue() * this.scaleFactor;
    g.fillRect(
            this.x + this.settings.barPadding, maxBarHeight - actualBarHeight, barWidth,
            actualBarHeight);
}