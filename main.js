// <editor-fold desc="CustomGameLoop">
function CustomGameLoop() {
}

CustomGameLoop.prototype = new GameLoop();

CustomGameLoop.prototype.initialize = function(canvas) {
    GameLoop.prototype.initialize.call(this, canvas);
    this.uiManager = new UIManager();
    this.canvas.width = 640;
    this.canvas.height = 480;
}

CustomGameLoop.prototype.onPointerActivate = function(id, position) {
    this.uiManager.onPointerActivate(id, position);
}

CustomGameLoop.prototype.addUIElement = function (element) {
    this.uiManager.addElement(element);
}

CustomGameLoop.prototype.draw = function(g) {
    this.uiManager.draw(g);
}
// </editor-fold> 

// <editor-fold desc="UIManager">
function UIManager() {
    this.elements = [];
}

UIManager.prototype.addElement = function(element) {
    this.elements.push(element);
}

UIManager.prototype.draw = function(g) {
    for (var i = 0; i < this.elements.length; i++) {
        this.elements[i].draw(g);
    }
}

UIManager.prototype.onPointerActivate = function(id, p) {
    
}
// </editor-fold>

// <editor-fold desc="UIElement">
function UIElement() {
    this.width = 0;
    this.height = 0;
    this.position = new Point(0, 0);
    this.backgroundColor = "#CCCCCC";
    this.children = [];
}

UIElement.prototype.setWidth = function(width) {
    this.width = width;
}

UIElement.prototype.setHeight = function(height) {
    this.height = height;
}

UIElement.prototype.setPosition = function(position) {
    this.position = position.clone();
}

UIElement.prototype.getPosition = function() {
    return this.position.clone();
}

UIElement.prototype.setBackgroundColor = function(color) {
    this.backgroundColor = color;
}

UIElement.prototype.draw = function(g) {
    g.save();
    g.translate(this.position.getX(), this.position.getY());
    g.fillStyle = this.backgroundColor;
    g.fillRect(0, 0, this.width, this.height);
    for (var i = 0; i < this.children.length; i++) {
        this.children[i].draw(g);
    }
    g.restore();
}

UIElement.prototype.addChild = function(element) {
    this.children.push(element);
}

UIElement.prototype.propagateOnPointerActive = function(e) {
    
}

UIElement.prototype.onPreviewPointerActive = function(e) {
    
}

UIElement.prototype.onPointerActive = function(e) {
    
}

UIElement.prototype.hitTest = function(p) {
    
}

UIElement.prototype.parentPointToLocal = function(p) {
    
}
// </editor-fold>

// <editor-fold desc="PointerEvent">
function PointerEvent (id, position) {
    this.id = id;
    this.position = position;
    this.isPropagationCancelled = false;
}

PointerEvent.prototype.getIsPropagationCancelled = function() {
    return this.isPropagationCancelled;
}

PointerEvent.prototype.cancelPropagation = function() {
    this.isPropagationCancelled = true;
}
// </editor-fold>

function initialize() {
    var customGameLoop = new CustomGameLoop();
    customGameLoop.initialize(document.getElementById("canvas"));
    var bluePanel = new UIElement();
    bluePanel.setPosition(new Point(60, 60));
    bluePanel.setWidth(400);
    bluePanel.setHeight(300);
    bluePanel.setBackgroundColor("blue");
    
    var redPanel = new UIElement();
    redPanel.setPosition(new Point(10, 10));
    redPanel.setWidth(200);
    redPanel.setHeight(200);
    redPanel.setBackgroundColor("red");
    bluePanel.addChild(redPanel);
    
    var greenPanel = new UIElement();
    greenPanel.setPosition(new Point(10, 10));
    greenPanel.setWidth(100);
    greenPanel.setHeight(20);
    greenPanel.setBackgroundColor("green");
    redPanel.addChild(greenPanel);
    
    var yellowPanel = new UIElement();
    yellowPanel.setPosition(new Point(10, 40));
    yellowPanel.setWidth(100);
    yellowPanel.setHeight(20);
    yellowPanel.setBackgroundColor("yellow");
    redPanel.addChild(yellowPanel);
    
    customGameLoop.addUIElement(bluePanel);
    
    console.info((typeof customGameLoop.uiManager.elements[0].asdfasdfasd == "undefined"));
}

window.onload = initialize;