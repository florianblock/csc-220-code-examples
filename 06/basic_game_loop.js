var CANVAS = null;
var G = null;
var MOUSE_POSITION = { x: 0, y: 0 };
var INTERACTIVE_CIRCLE = null;

function InteractiveCircle() {
    this.position = { x: 0, y: 0 };
}

InteractiveCircle.prototype.updatePosition = function(x, y) {
    this.position.x = x;
    this.position.y = y;
}

InteractiveCircle.prototype.draw = function(g)
{
    g.fillStyle = "red";
    g.beginPath();
    g.arc(this.position.x, this.position.y, 50, 0, Math.PI * 2);
    g.fill();
}

function initializeGraphics() {
    CANVAS = document.getElementById("canvas");
    CANVAS.width = 600;
    CANVAS.height = 400;
    G = CANVAS.getContext("2d");
}

function initializeInput() {
    CANVAS.onmousemove = onMouseMove;
}

function onMouseMove(e) {
    var offset = this.getBoundingClientRect();
    MOUSE_POSITION.x = e.clientX - offset.left;
    MOUSE_POSITION.y = e.clientY - offset.top;
}

function initializeTimer() {
    setInterval(onGameLoopTick, 10);
}

function onGameLoopTick() {
    processInput();
    update();
    draw();
}

function processInput() {
    // nothing required
}

function update() {
    INTERACTIVE_CIRCLE.updatePosition(MOUSE_POSITION.x, MOUSE_POSITION.y);
}

function draw() {
    G.fillStyle = "white";
    G.fillRect(0, 0, CANVAS.width, CANVAS.height);
    INTERACTIVE_CIRCLE.draw(G);
}

function initializeGameLoop() {
    INTERACTIVE_CIRCLE = new InteractiveCircle();
    initializeGraphics();
    initializeInput();
    initializeTimer();
}

window.onload = initializeGameLoop;