function InteractiveCircle() {
    this.position = { x: 0, y: 0 };
}

InteractiveCircle.prototype.updatePosition = function(position) {
    this.position.x = position.x;
    this.position.y = position.y;
}

InteractiveCircle.prototype.draw = function(g)
{
    g.fillStyle = "red";
    g.beginPath();
    g.arc(this.position.x, this.position.y, 50, 0, Math.PI * 2);
    g.fill();
}

function InteractiveCircleDemo() {
    this.circle = new InteractiveCircle();
}

InteractiveCircleDemo.prototype = new GameEngine();

InteractiveCircleDemo.prototype.onMouseMove = function(position) {
    this.circle.updatePosition(position);
}

InteractiveCircleDemo.prototype.draw = function(g) {
    this.circle.draw(g);
}

function initialize() {
    var demo = new InteractiveCircleDemo();
    demo.initialize(document.getElementById("canvas"));
}

window.onload = initialize;