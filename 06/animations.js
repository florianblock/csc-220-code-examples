var ANIMATION_FUNCTIONS = {
    line : function(progress) {
        return progress;
    },
    
    pow : function(progress) {
        return Math.pow(progress, 2);
    },
    
    circle : function(progress) {
        return 1 - Math.sin(Math.acos(progress));
    },
    
    bow : function(progress) {
        return Math.pow(progress, 2) * ((x + 1) * progress - x);
    },
    
    bounce : function(progress) {
        for(var a = 0, b = 1, result; 1; a += b, b /= 2) {
            if (progress >= (7 - 4 * a) / 11) {
              return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + 
                      Math.pow(b, 2)
            }
        }
    },
    
    elastic : function(progress) {
        var x = 1.5;
        return Math.pow(2, 10 * (progress-1)) * Math.cos(20 * Math.PI * x / 3 * 
                progress);
    }
}

function invertAnimationFunction(delta) { 
    return function(progress) {
        return 1 - delta(1 - progress)
    }
}


function InteractiveCircle() {
    this.position = { x: 50, y: 200 };
}

InteractiveCircle.prototype.updatePosition = function(position) {
    this.position.x = position.x;
    this.position.y = position.y;
}

InteractiveCircle.prototype.draw = function(g)
{
    g.fillStyle = "red";
    g.beginPath();
    g.arc(this.position.x, this.position.y, 50, 0, Math.PI * 2);
    g.fill();
}

function AnimationDemo() {
    this.circle = new InteractiveCircle();
    this.elapsedTimeInMS = 0;
    this.animationDuration = 5000;
    this.animationFunction = function(progress) {
        return progress;
    };
    this.animationFunction = ANIMATION_FUNCTIONS.bounce;
}

AnimationDemo.prototype = new GameEngine();

AnimationDemo.prototype.update = function(elapsedMilliseconds) {
    this.elapsedTimeInMS += elapsedMilliseconds;
    var progress = this.elapsedTimeInMS / this.animationDuration;
    this.circle.updatePosition(
            {
                x : 50 + 500 * this.animationFunction(progress),
                y : 200
            });
}

AnimationDemo.prototype.draw = function(g) {
    this.circle.draw(g);
}

function initialize() {
    var demo = new AnimationDemo();
    demo.initialize(document.getElementById("canvas"));
}

window.onload = initialize;