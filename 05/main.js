// create temporary / helper objects
var person = {
    name : "",
    walk : function() {}
}

// assign properties / functions to an individual object
person.title = ""; 
person.sit = function() {};

// Constructor based instantiation for object-oriented programming
function Person() {
    this.name = "";
}

// instantiate instance of Person
var person = new Person();

// set up function
Person.prototype.walk = function() {
}

// assign properties / functions to an individual object
person.title = ""; 
person.sit = function() {};

var person2 = new Person(); // does not have sit() or title

Person.prototype.sit = function() {
}

function Person() {
    this.name = "";
    this.sit = function() {};
}

function Employee() {
    this.ID = "";
}

Employee.prototype = new Person();

Employee.prototype.getPaid = function() {
}

var employee = new Employee();
employee.walk();

var name = employee.name;
// name is empty string

employee.name = "Suzie";

function Person() {
    this.coordinates = { x : 0, y : 0 };
}

function Employee() {
}

Employee.prototype = new Person();

var employee = new Employee();
var coordinates = employee.coordinates;
// coordinates is { 0, 0 }
employee.coordinates = { x: 10, y : 20 };

function Person() {
    this.coordinates = { x : 0, y : 0 };
}

function Employee() {
}

Employee.prototype = new Person();

var employee = new Employee();
employee.coordinates.x = 10;

// what employee.coordinates.x = actually does:
var temp = employee.coordinates;
temp.x = 10;

function Person() {
}

Person.prototype.sit = function() {
}

function YogaInstructor() {
}

YogaInstructor.prototype = new Person();
YogaInstructor.prototype.sit = function() {
    // sit in a really cool way
}

function DataPoint() {
    this.label = "";
    this.x = 0;
    this.y = 0;
}

DataPoint.prototype.getScaledPoint = function(scaleFactor) {
    return { x : this.x * scaleFactor, y : this.y * scaleFactor };
}

function RoundedDataPoint() {
    // call base constructor
    DataPoint.call(this);
}

RoundedDataPoint.prototype = new DataPoint();

RoundedDataPoint.prototype.getScaledPoint = function(scaleFactor) {
    var point = DataPoint.prototype.getScaledPoint.call(this, scaleFactor);
    point.x = Math.round(point.x);
    point.y = Math.round(point.y);
    return point;
}

function getVectorLength(x, y) {
    
}

function getVectorLength(vector) {
    
}

function getVectorLength(arg1, arg2) {
    if (typeof arg1 == "string" && typeof arg2 == "undefined") {
    }
    else if (typeof arg1 == "number" && typeof arg2 == "number") {
    }
}

function getVectorLength(args) {
    if (args.length == 1) { 
    }
    else if (args.length == 2) {
    }
}

getVector([ 10, 20]);

function getVectorLength(args) {
    if (typeof args.vector != 'undefined') {
    }
    else if (typeof args.x != "undefined" && typeof args.y != "undefined") {  
    }
}

getVector({ vector: new Vector(5, 5) });

function DemoObject() {
    this.x = 0;
    this.y = 0;
}

DemoObject.prototype.getSum = function() {
    return this.x + this.y;
}

DemoObject.prototype.setValues = function(x, y) {
    this.x = x;
    this.y = y;
}

var o = DemoObject();
o.setValues(10, 20); // Amazing
var sum = getSum(); // Amazing
o.x = 10; // Terrible
o.y = 10; // Terrible
sum = o.x + o.y; // Terrible

function DemoObject(point) {
    if (typeof point == 'undefined') {
        
    } else {
        this.length = point.getLength();
    }
}

function OtherObject() {   
}

OtherObject.prototype = new DemoObject();

function initialize()
{
}

window.onload = initialize;
