// <editor-fold desc="CustomGameLoop">
function CustomGameLoop() {
    
}

CustomGameLoop.prototype = new GameLoop();

CustomGameLoop.prototype.initialize = function(canvas) {
    GameLoop.prototype.initialize.call(this, canvas);
    this.inputManager = new InputManager();
}

CustomGameLoop.prototype.onPointerEnter = function(id, position) {
    this.inputManager.onPointerEnter(id, position);
}

CustomGameLoop.prototype.onPointerMove = function(id, position) {
    this.inputManager.onPointerMove(id, position);
}

CustomGameLoop.prototype.onPointerActivate = function(id, position) {
    this.inputManager.onPointerActivate(id, position);
}

CustomGameLoop.prototype.onPointerDeactivate = function(id, position) {
    this.inputManager.onPointerDeactivate(id, position);
}

CustomGameLoop.prototype.onPointerLeave = function(id, position) {
    this.inputManager.onPointerLeave(id, position);
}

CustomGameLoop.prototype.draw = function(g) {
    g.fillStyle = "lightgray";
    g.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.inputManager.drawPointerDebugOverlay(g);
}

CustomGameLoop.prototype.drawPoint = function(g, p, r, color) {
    g.beginPath();
    g.arc(p.getX(), p.getY(), r, 0, Math.PI * 2);
    g.fillStyle = color;
    g.fill();
}
// </editor-fold>

// <editor-fold desc="InputManager">
function InputManager() {
    this.pointers = { };
    this.pointerCount = 0;
    this.pivot = new Point(0, 0);
    this.previousPivot = new Point(0, 0);
    this.pivotRadius = 0;
    this.previousPivotRadius = 0;
    this.debugTransformationMatrix = Matrix.getTranslate(0, 0);
}

InputManager.prototype.onPointerEnter = function(id, position) {
    this.addPointer(id, position);
    if (id != GameLoop.Settings.Input.MOUSE_ID) {
        if (this.hasPointer(GameLoop.Settings.Input.MOUSE_ID)) {
            this.onPointerLeave(GameLoop.Settings.Input.MOUSE_ID, 
                new Point(0, 0));
        }
    }
}

InputManager.prototype.onPointerMove = function(id, position) {
    if (this.hasPointer(id)) {
        this.movePointer(id, position);
    }
}

InputManager.prototype.onPointerActivate = function(id, position) {
    this.pointers[id].activate();
}

InputManager.prototype.onPointerDeactivate = function(id, position) {
    if (this.hasPointer(id)) {
        this.pointers[id].deactivate();
    }
}

InputManager.prototype.onPointerLeave = function(id, position) {
    if (this.hasPointer(id)) {
        this.removePointer(id, position);
    }
}

InputManager.prototype.hasPointer = function(id) {
    return typeof this.pointers[id] != 'undefined';
}

InputManager.prototype.addPointer = function(id, initialPosition) {
    this.pointers[id] = new Pointer(id, initialPosition);
    this.pointerCount++;
    this.updatePivot();
}

InputManager.prototype.movePointer = function(id, position) {
    this.pointers[id].move(position);
    this.updatePivot();
    this.onManipulation(this.pointers[id]);
}

InputManager.prototype.removePointer = function(id, position) {
    delete this.pointers[id];
    this.pointerCount--;
    this.updatePivot();
}

InputManager.prototype.drawPointerDebugOverlay = function(g) {
    g.save();
    g.setTransform(
            this.debugTransformationMatrix.get(0, 0),
            this.debugTransformationMatrix.get(0, 1),
            this.debugTransformationMatrix.get(1, 0),
            this.debugTransformationMatrix.get(1, 1),
            this.debugTransformationMatrix.get(2, 0),
            this.debugTransformationMatrix.get(2, 1)
            );
    g.fillStyle = "red";
    g.fillRect(0, 0, 400, 200);
    g.restore();
    for (var id in this.pointers) {
        this.pointers[id].drawDebugOverlay(g);
    }
    if (this.pointerCount > 0) {
        g.beginPath();
        g.strokeStyle = "green";
        g.lineWidth = 1;
        g.arc(this.pivot.getX(), this.pivot.getY(), 10, 0, Math.PI * 2);
        g.stroke();
        g.strokeStyle = "gray";
        g.beginPath();
        g.arc(this.pivot.getX(), this.pivot.getY(), this.pivotRadius, 
            0, Math.PI * 2);
        g.stroke();
    }
    g.fillStyle = "gray";
    g.font = "10px Arial";
    g.fillText("Pointers: " + this.pointerCount, 5, 12);
}

InputManager.prototype.updatePivot = function() {
    this.previousPivot = this.pivot.clone();
    this.previousPivotRadius = this.pivotRadius;
    if (this.pointerCount > 0) {
        var xAverage = 0;
        var yAverage = 0;
        for (var id in this.pointers) {
            xAverage += this.pointers[id].getPosition().getX();
            yAverage += this.pointers[id].getPosition().getY();
        }
        xAverage /= this.pointerCount;
        yAverage /= this.pointerCount;
        this.pivot = new Point(xAverage, yAverage);
        var distanceAverage = 0;
        for (var id in this.pointers) {
            distanceAverage += this.pointers[id].getPosition()
                    .subtract(this.pivot).getLength();
        }
        distanceAverage /= this.pointerCount;
        this.pivotRadius = distanceAverage;
    } else {
        this.pivot = new Point(0, 0);
    }
}

InputManager.prototype.onManipulation = function(movedPointer) {
    // translation
    var xTranslation = this.pivot.getX() - this.previousPivot.getX();
    var yTranslation = this.pivot.getY() - this.previousPivot.getY();
    this.debugTransformationMatrix = 
            Matrix.getTranslate(xTranslation, yTranslation)
                .multiply(this.debugTransformationMatrix);
    if (this.previousPivotRadius > 0) {
        // scale
        var scale = this.pivotRadius / this.previousPivotRadius;
        this.debugTransformationMatrix = 
                Matrix.getScaleAt(scale, scale, this.pivot.getX(), 
                    this.pivot.getY()).multiply(this.debugTransformationMatrix);
//                Matrix.getScale(scale, scale).multiply(this.debugTransformationMatrix);
        // rotate
        var previousDirection = 
                movedPointer.getPreviousPosition().subtract(this.pivot);
        var currentDirection =
                movedPointer.getPosition().subtract(this.pivot);
//        
        var angle = 
                (Math.atan2(previousDirection.getX(), 
                    previousDirection.getY()) - 
                Math.atan2(currentDirection.getX(), currentDirection.getY())) /
                this.pointerCount;
        this.debugTransformationMatrix =
                Matrix.getRotationAt(angle, 
                    this.pivot.getX(), this.pivot.getY()).multiply(
                            this.debugTransformationMatrix);
//        var test = true;
    }
}
// </editor-fold>

// <editor-fold desc="Pointer">
function Pointer(id, initialPosition) {
    this.id = id;
    this.position = initialPosition.clone();
    this.previousPosition = initialPosition.clone();
    this.isActive = false;
}

Pointer.prototype.move = function(position) {
    this.previousPosition = this.position.clone();
    this.position.setX(position.getX());
    this.position.setY(position.getY());
}

Pointer.prototype.getPosition = function() {
    return this.position.clone();
}

Pointer.prototype.getPreviousPosition = function() {
    return this.previousPosition.clone();
}

Pointer.prototype.getIsActive = function() {
    return this.isActive;
}

Pointer.prototype.drawDebugOverlay = function(g) {
    g.strokeStyle = "black";
    g.fillStyle = "black";
    g.font = "10px Arial"
    g.lineWidth = this.getIsActive() ? 3 : 1;
    g.globalAlpha = this.getIsActive() ? 1 : 0.5;
    var position = this.getPosition();
    g.beginPath();
    g.rect(position.getX() - 20, position.getY() - 20, 40, 40);
    g.stroke();
    g.fillText(this.id, position.getX() - 20, position.getY() - 20 - 3);
    g.globalAlpha = 1.0;
}

Pointer.prototype.activate = function() {
    this.isActive = true;
}

Pointer.prototype.deactivate = function() {
    this.isActive = false;
}
// </editor-fold>

function initialize() {
    var point = new Point(1, 0);
    console.info(point.toString());
    var transformation = Matrix.getTranslate(1, 0).multiply(
            Matrix.getRotation(Math.PI / 2));
    console.info(transformation.toString());
    var translatedPoint = transformation.multiply(point);
    console.info(translatedPoint.toString());
//    var 
    var customGameLoop = new CustomGameLoop();
    customGameLoop.initialize(document.getElementById("canvas"));
}

window.onload = initialize;