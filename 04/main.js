var CANVAS = null;
var G = null;

function initializeGraphics()
{
    CANVAS = document.getElementById("canvas");
    CANVAS.width = 600;
    CANVAS.height = 400;
    G = CANVAS.getContext("2d");
}

function initializeInput()
{
    CANVAS.onmousemove = onMouseMove;
}

function onMouseMove(e)
{
    var offset = this.getBoundingClientRect();
    console.info((e.clientX - offset.left) + "; " + (e.clientY - offset.top));
}

function drawSample1()
{
    G.fillStyle = "green";
    G.fillRect(0, 0, CANVAS.width, CANVAS.height);
}

function drawSample2()
{
    G.fillStyle = "white";
    G.strokeStyle = "black";
    G.lineWidth = 5;
    G.rect(50, 50, 200, 200);
    G.fillStyle = "blue";
    G.fill();
    G.save();
        G.clip();
        G.fillStyle = "green";
        G.beginPath();
        G.arc(250, 250, 100, 0, Math.PI * 2);
        G.fill();
    G.restore();
    G.beginPath();
    G.rect(50, 50, 200, 200);
    G.stroke();
}

function initialize()
{
    initializeGraphics();
//    initializeInput();
//    drawSample1();
    drawSample2();
}

window.onload = initialize;
