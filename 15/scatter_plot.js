function ManipulableVisual() {
    this.transformation = Matrix.getTranslate(0, 0);
    this.inverseTransformation = Matrix.getTranslate(0, 0);
    this.inputManager = null;
}

ManipulableVisual.prototype.initializeInput = function(manager) {
    this.inputManager = manager;
    this.inputManager.setManipulationTarget(this);
}

ManipulableVisual.prototype.scaleAt = function(s, p) {
    this.transformation = 
            Matrix.getScaleAt(s, s, p.getX(), 
                p.getY()).multiply(this.transformation);
    this.updateInverse();
}

ManipulableVisual.prototype.translate = function(dx, dy) {
    this.transformation = 
            Matrix.getTranslate(dx, dy)
                .multiply(this.transformation);
    this.updateInverse();
}

ManipulableVisual.prototype.updateInverse = function() {
    this.inverseTransformation = this.transformation.getInverse();
}

ManipulableVisual.prototype.screenPointToLocal = function(p) {
    return this.inverseTransformation.multiply(p);
}

ManipulableVisual.prototype.localPointToScreen = function(p) {
    return this.transformation.multiply(p);
}

function ScatterPlot() {
    ManipulableVisual.call(this);
    this.width = 1000;
    this.height = 1000;
    this.data = [];
}

ScatterPlot.prototype = new ManipulableVisual();

ScatterPlot.prototype.addData = function(data) {
    this.data.push(data);
}

ScatterPlot.prototype.draw = function(g) {
    g.save();
    this.transformation.setGraphicsTransformation(g);
    g.beginPath();
    g.rect(0, 0, this.width, this.height);
    g.clip();
    this.drawCore(g);
    if (CustomGameLoop.isHoverEnabled) {
        this.drawHoverElements(g);
    }
    g.restore();
}

ScatterPlot.prototype.drawHoverElements = function(g) {
    g.save();
    g.resetTransform();
    g.fillStyle = "green";
    var pointers = this.inputManager.getPointers();
    for (var i = 0; i < pointers.length; i++) {
        var pointer = pointers[i];
        var localPoint = this.screenPointToLocal(pointer.getPosition());
        var point = this.getPointFromPosition(localPoint);
        if (point != null) {
            this.drawDot(g, this.localPointToScreen(point), 30);
        }
    }
    g.restore();
}

ScatterPlot.prototype.drawCore = function(g) {
    g.fillStyle = "lightgray";
    g.fillRect(0, 0, this.width, this.height);
    this.drawPoints(g);
}

ScatterPlot.prototype.drawPoints = function(g) {
    for (var i = 0; i < this.data.length; i++) {
        this.drawPointData(g, this.data[i])
    }
}

ScatterPlot.prototype.drawPointData = function(g, pointData) {
    g.fillStyle = pointData.getColor();
    for (var j = 0; j < pointData.getPoints().length; j++) {
        this.drawDot(g, pointData.getPoints()[j], 1);
    }
}

ScatterPlot.prototype.drawDot = function(g, p, r) {
    g.beginPath();
    g.arc(p.getX(), p.getY(), r, 0, Math.PI * 2);
    g.fill();
}

ScatterPlot.prototype.getPointFromPosition = function(p) {
    for (var i = 0; i < this.data.length; i++) {
        for (var j = 0; j < this.data[i].getPoints().length; j++) {
            var point = this.data[i].getPoints()[j];
            if (this.pointHitTest(p, point)) {
                return point;
            }
        }
    }
    return null;
}

ScatterPlot.prototype.pointHitTest = function(pointerPosition, point) {
    var hitBoxTolerance = 3;
    var hitBox = 
            new Rect(
                point.getX() - hitBoxTolerance,
                point.getY() - hitBoxTolerance,
                hitBoxTolerance * 2,
                hitBoxTolerance * 2
                );
    return hitBox.containsPoint(pointerPosition);
}

function AccelleratedScatterPlot(bufferSize) {
    ScatterPlot.call(this);
    this.initializeRenderBuffer(bufferSize);
}

AccelleratedScatterPlot.bufferSize = 1000;

AccelleratedScatterPlot.prototype = new ScatterPlot();

AccelleratedScatterPlot.prototype.initializeRenderBuffer = 
function(bufferSize) {
    this.renderBuffer = document.createElement("canvas");
    this.bufferSize = bufferSize;
    this.renderBuffer.width = bufferSize;
    this.renderBuffer.height = bufferSize;
    this.bufferG = this.renderBuffer.getContext("2d");
    this.bufferSizeRatio = bufferSize / this.width;
    this.bufferG.scale(this.bufferSizeRatio, this.bufferSizeRatio);
    this.bufferG.fillStyle = "lightgray";
    this.bufferG.fillRect(0, 0, this.renderBuffer.width, 
        this.renderBuffer.height);
}

AccelleratedScatterPlot.prototype.addData = function(data) {
    ScatterPlot.prototype.addData.call(this, data);
    this.drawPointData(this.bufferG, data);
}

AccelleratedScatterPlot.prototype.drawCore = function(g) {
    g.drawImage(this.renderBuffer, 0, 0, this.width, this.height);
}

function QuadScatterPlot(depth) {
    ScatterPlot.call(this);
    this.depth = depth;
    this.tileSize = 1000;
}

QuadScatterPlot.prototype = new ScatterPlot();

QuadScatterPlot.prototype.drawCore = function(g) {
    g.fillStyle = "lightgray";
    g.fillRect(0, 0, this.width, this.height);
    var scale = this.transformation.getScale();
    var translation = this.transformation.getTranslation();
    var screenBounds = new Rect(
            translation.x, translation.y, 
            this.width * scale, this.height * scale)
    this.root.draw(g, screenBounds);
    if (CustomGameLoop.isVisualDebugEnabled) {
        this.root.drawDebug(g);
    }
}

QuadScatterPlot.prototype.preRender = function() {
    this.initializeQuadTree();
    this.initializeBufferLevels();
}

QuadScatterPlot.prototype.initializeQuadTree = function() {
    this.root = new QuadTreeNode(
            new Rect(0, 0, this.width, this.height), 1);
    this.initializeQuadTreeRecursive(
            this.root, this.depth);
    for (var i = 0; i < this.data.length; i++) {
        var pointData = this.data[i];
        for (var j = 0; j < pointData.getPoints().length; j++) {
            this.root.drawPoint(
                    pointData.getPoints()[j], 1, pointData.getColor());
            this.root.indexPoint(pointData.getPoints()[j]);
        }
    }
}

QuadScatterPlot.prototype.initializeQuadTreeRecursive = 
function(node, remainingDepth) {
    if (remainingDepth > 0) {
        var halfSize = node.getBounds().getWidth() / 2;
        for (var xSector = 0; xSector < 2; xSector++) {
            for (var ySector = 0; ySector < 2; ySector++) {
                var child = new QuadTreeNode(
                        new Rect(
                            node.getBounds().getX() + xSector * halfSize,
                            node.getBounds().getY() + ySector * halfSize,
                            halfSize, halfSize), node.getDepth() + 1);
                node.addChild(child, xSector, ySector);
                this.initializeQuadTreeRecursive(child, remainingDepth - 1);
            }
        }
    }
}

QuadScatterPlot.prototype.initializeBufferLevels = function() {
    this.root.generateBuffer();
}

QuadScatterPlot.prototype.getPointFromPosition = function(p) {
    var quadrantPointIndex = this.root.getPointIndex(p);
    if (quadrantPointIndex == null) {
        return null;
    }
    for (var i = 0; i < quadrantPointIndex.length; i++) {
        if (this.pointHitTest(p, quadrantPointIndex[i])) {
            return quadrantPointIndex[i];
        }
    }
}

function QuadTreeNode(bounds, depth) {
    this.depth = depth;
    this.bounds = bounds;
    this.children = [];
    this.renderBuffer = document.createElement("canvas");
    this.renderBuffer.width = QuadTreeNode.quadrantSize;
    this.renderBuffer.height = QuadTreeNode.quadrantSize;
    this.scaleFactor = Math.pow(2, (this.depth - 1));
    this.bufferG = this.renderBuffer.getContext("2d");
    this.bufferG.scale(this.scaleFactor, this.scaleFactor);
    this.bufferG.translate(-this.bounds.getX(), -this.bounds.getY());
    this.nodeIndex = [];
}

QuadTreeNode.quadrantSize = 1000;
QuadTreeNode.imageDrawCounter = 0;

QuadTreeNode.prototype.getDepth = function() {
    return this.depth;
}

QuadTreeNode.prototype.getBounds = function() {
    return this.bounds;
}

QuadTreeNode.prototype.getRenderBuffer = function() {
    return this.renderBuffer;
}

QuadTreeNode.prototype.addChild = function(child, xSector, ySector) {
    this.children[this.
                getChildIndexBySectorCoordinates(xSector, ySector)] = child;
}

QuadTreeNode.prototype.getChild = function(xSector, ySector) {
    return this.children[this.
                getChildIndexBySectorCoordinates(xSector, ySector)];
}

QuadTreeNode.prototype.getChildIndexBySectorCoordinates = 
function(xSector, ySector) {
    return xSector * 2 + ySector;
}

QuadTreeNode.prototype.drawDebug = function(g) {
    g.lineWidth = 1;
    g.strokeStyle = "gray";
    g.beginPath();
    g.rect(this.bounds.x, this.bounds.y, this.bounds.w, this.bounds.h);
    g.stroke();
    for (var i = 0; i < this.children.length; i++) {
        this.children[i].drawDebug(g);
    }
}

QuadTreeNode.prototype.drawPoint = function(p, radius, color) {
    if (this.children.length == 0) {
        this.bufferG.fillStyle = color;
        this.bufferG.beginPath();
        this.bufferG.arc(p.getX(), p.getY(), radius, 0, Math.PI * 2);
        this.bufferG.fill();
    } else {
        var pointBounds = new Rect(
                p.getX() - radius, 
                p.getY() - radius,
                radius * 2,
                radius * 2);
        for (var i = 0; i < this.children.length; i++) {
            if (this.children[i].overlapsWithRect(pointBounds)) {
                this.children[i].drawPoint(p, radius, color);
            }
        }
    }
}

QuadTreeNode.prototype.indexPoint = function(p) {
    if (this.children.length == 0) {
        this.nodeIndex.push(p);
    } else {
        for (var i = 0; i < this.children.length; i++) {
            if (this.children[i].containsPoint(p)) {
                this.children[i].indexPoint(p);
                return;
            }
        }
    }
}

QuadTreeNode.prototype.getPointIndex = function(p) {
    if (this.children.length == 0) {
        return this.nodeIndex;
    } else {
        for (var i = 0; i < this.children.length; i++) {
            if (this.children[i].containsPoint(p)) {
                return this.children[i].getPointIndex(p);
            }
        }
    }
    return null; 
}

QuadTreeNode.prototype.containsPoint = function(p) {
    return this.bounds.containsPoint(p);
}

QuadTreeNode.prototype.overlapsWithRect = function(r) {
    return this.bounds.overlapsWithRect(r);
}

QuadTreeNode.prototype.draw = function(g, screenBounds) {
    if (screenBounds.getWidth() <= QuadTreeNode.quadrantSize ||
            this.children.length == 0) {
        if (screenBounds.overlapsWithRect(new Rect(0, 0, 1000, 1000))) {
            g.drawImage(this.renderBuffer, 
                this.bounds.getX(), this.bounds.getY(),
                this.bounds.getWidth(), this.bounds.getHeight());
        }
    } else {
        var halfScreenBoundsSize = screenBounds.getWidth() / 2;
        for (var xSector = 0; xSector < 2; xSector++) {
            for (var ySector = 0; ySector < 2; ySector++) {
                var childScreenBounds = new Rect(
                        screenBounds.getX() + halfScreenBoundsSize * xSector,
                        screenBounds.getY() + halfScreenBoundsSize * ySector,
                        halfScreenBoundsSize, halfScreenBoundsSize
                        );
                this.getChild(xSector, ySector).draw(g, childScreenBounds);
            }
        }
    }
}

QuadTreeNode.prototype.drawWithoutBufferLevels = function(g, screenBounds) {
    if (this.children.length == 0) {
        g.drawImage(this.renderBuffer, 
            this.bounds.getX(), this.bounds.getY(),
            this.bounds.getWidth(), this.bounds.getHeight());
    } else {
        for (var i = 0; i < this.children.length; i++) {
            this.children[i].draw(g);
        }
    }
}

QuadTreeNode.prototype.generateBuffer = function() {
    if (this.children.length > 0) {
        for (var i = 0; i < this.children.length; i++) {
            this.children[i].generateBuffer();
            var childBounds = this.children[i].getBounds();
            this.bufferG.drawImage(
                    this.children[i].getRenderBuffer(),
                    childBounds.getX(), childBounds.getY(),
                    childBounds.getWidth(), childBounds.getHeight());
        }
    }
}

function Rect(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
}

Rect.prototype.getX = function() {
    return this.x;
}

Rect.prototype.getY = function() {
    return this.y;
}

Rect.prototype.getWidth = function() {
    return this.w;
}

Rect.prototype.getHeight = function() {
    return this.h;
}

Rect.prototype.containsPoint = function(p) {
    return this.x < p.getX() && p.getX() < this.x + this.w &&
            this.y < p.getY() && p.getY() < this.y + this.h;
}

Rect.prototype.overlapsWithRect = function(r) {
    return !(r.getX() + r.getWidth() < this.x ||
            this.x + this.w < r.getX() ||
            r.getY() + r.getHeight() < this.y ||
            this.y + this.h < r.getY());
}



