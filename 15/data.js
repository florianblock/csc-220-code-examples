function PointData() {
    this.color = 'red';
    this.points = [];
}

PointData.prototype.setColor = function(color) {
    this.color = color;
}

PointData.prototype.getColor = function() {
    return this.color;
}

PointData.prototype.addPoint = function(p) {
    this.points.push(p);
}

PointData.prototype.getPoints = function() {
    return this.points;
}