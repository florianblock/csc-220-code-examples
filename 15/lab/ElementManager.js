function ElementManager() {
    this.elements = [];
}

ElementManager.prototype.addElement = function(element) {
    this.elements.push(element);
}

ElementManager.prototype.draw = function(g) {
    for (var i = 0; i < this.elements.length; i++) {
        this.elements[i].draw(g);
    }
}

ElementManager.prototype.scrollAllStarFields = function(delta) {
    for (var i = 0; i < this.elements.length; i++) {
        if (typeof this.elements[i]["scrollVertically"] === 'function') {
            this.elements[i].scrollVertically(delta);
        }
    }
}


