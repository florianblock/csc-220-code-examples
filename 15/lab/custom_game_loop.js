// <editor-fold desc="CustomGameLoop">
function CustomGameLoop() {
    
}

CustomGameLoop.prototype = new GameLoop();

CustomGameLoop.prototype.initialize = function(canvas) {
    GameLoop.prototype.initialize.call(this, canvas);
    this.canvas.width = 1000;
    this.canvas.height = 500;
    this.elementManager = new ElementManager();
    this.elementManager.addElement(
            new StarField(
                this.canvas.width, this.canvas.height, 100, 3, 1));
    this.elementManager.addElement(
            new StarField(
                this.canvas.width, this.canvas.height, 1000, 1, 0.5));
    this.elementManager.addElement(
            new StarField(
                this.canvas.width, this.canvas.height, 10000, 0.5, 0.25));
    this.elementManager.addElement(
            new StarField(
                this.canvas.width, this.canvas.height, 100000, 0.2, 0.125));
    this.elementManager.addElement(new Spaceship());
    // add initialization here
    this.previousPositions = {};
}

CustomGameLoop.prototype.draw = function(g) {
    g.fillStyle = "black";
    g.fillRect(0, 0, this.canvas.width, this.canvas.height);
    // add drawing here
        this.elementManager.draw(g);
}

CustomGameLoop.prototype.onPointerMove = function(id, position) {
    if (typeof this.previousPositions[id] === 'undefined') {
    } else {
        var delta = position.subtract(this.previousPositions[id]);
        this.elementManager.scrollAllStarFields(delta.getY());
    }
    this.previousPositions[id] = position;
}

CustomGameLoop.prototype.onPointerLeave = function(id, position) {
    delete this.previousPositions[id];
}
// </editor-fold>

function initialize() {
    var customGameLoop = new CustomGameLoop();
    customGameLoop.initialize(document.getElementById("canvas"));
}

window.onload = initialize;