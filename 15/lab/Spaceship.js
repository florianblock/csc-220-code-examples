function Spaceship() {
    Visual.call(this);
    this.image = new Image();
    this.image.src = "voyager.png";
    this.setPosition(new Point(300, 200));
}

Spaceship.prototype = new Visual();

Spaceship.prototype.draw = function(g) {
    if (this.image.complete) {
        g.drawImage(this.image, this.position.getX(), this.position.getY());
    }
}