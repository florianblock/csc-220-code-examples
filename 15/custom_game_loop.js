// <editor-fold desc="CustomGameLoop">
function CustomGameLoop() {
    
}

CustomGameLoop.isVisualDebugEnabled = false;
CustomGameLoop.isDebugZoomEnabled = false;
CustomGameLoop.isHoverEnabled = false;

CustomGameLoop.toggleVisualDebug = function() {
    CustomGameLoop.isVisualDebugEnabled = !CustomGameLoop.isVisualDebugEnabled;
}

CustomGameLoop.toggleDebugZoom = function() {
    CustomGameLoop.isDebugZoomEnabled = !CustomGameLoop.isDebugZoomEnabled;
}

CustomGameLoop.toggleHover = function() {
    CustomGameLoop.isHoverEnabled = !CustomGameLoop.isHoverEnabled;
}

CustomGameLoop.prototype = new GameLoop();

CustomGameLoop.prototype.generateFakePointData = function(f, n, color) {
    var returnData = new PointData();
    returnData.setColor(color);
    var step = 1000 / n;
    var randomSpread = 200;
    for (var i = 0; i < n; i++) {
        var x = i * step;
        var point = 
                new Point(
                    x + (Math.random() * randomSpread - randomSpread / 2), 
                    f(x) + (Math.random() * randomSpread - randomSpread / 2));
        returnData.addPoint(point);
    }
    return returnData;
}

CustomGameLoop.prototype.initialize = function(canvas) {
    GameLoop.prototype.initialize.call(this, canvas);
    this.initializeUI();
    this.canvas.width = 1000;
    this.canvas.height = 1000;
    this.inputManager = new InputManager();
    if (this.plotType == "modeDirect") {
        this.setUpScatterPlot();
    } else if (this.plotType == "modeBuffered") {
        this.setUpAccelleratedScatterPlot();
    } else if (this.plotType == "modeQuadTree") {
        this.setUpQuadScatterPlot();
    }
    this.scatterPlot.initializeInput(this.inputManager);
    document.addEventListener("keydown", 
        function(e) {
            if (e.keyCode == 68) { // D-key
                CustomGameLoop.toggleVisualDebug();
            } else if (e.keyCode == 90) { // Z-key
                CustomGameLoop.toggleDebugZoom();
            } else if (e.keyCode == 73) { // Z-key
                CustomGameLoop.toggleHover();
            }
        }, 
        false);
}

CustomGameLoop.prototype.initializeUI = function() {
    this.plotType = "modeDirect";
    
    if (this.getParameterByName("plottype") == "buffered") {
        this.plotType = "modeBuffered";
        AccelleratedScatterPlot.bufferSize = 
                Number(this.getParameterByName("bufferedsize"));
    } else if (this.getParameterByName("plottype") == "quadtree") {
        this.plotType = "modeQuadTree";
    }
    if (this.getParameterByName("bufferedsize") != "") {
        document.getElementById("bufferedSize").value = 
                AccelleratedScatterPlot.bufferSize;
    }
    
    if (this.getParameterByName("bufferlevelsdisabled") == "1") {
        document.getElementById("bufferLevelsDisabled").checked = true;
        QuadTreeNode.prototype.draw = 
                QuadTreeNode.prototype.drawWithoutBufferLevels;
    }
    document.getElementById(this.plotType).checked = true;
    var pointCountString = this.getParameterByName("countpoint");
    this.numberOfDataPoints = 1000;
    if (pointCountString != "") {
        this.numberOfDataPoints = Number(pointCountString);
    }
    document.getElementById("countPoint").value = this.numberOfDataPoints;
}

CustomGameLoop.prototype.getParameterByName = function(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

CustomGameLoop.prototype.setUpScatterPlot = function() {
    this.scatterPlot = new ScatterPlot();
    this.scatterPlot.addData(
            this.generateFakePointData(
                function(x) {
                    return Math.sin(x / 1000 * Math.PI) * 0.7 * 1000;
                },
                this.numberOfDataPoints, "red"
            )
        );
    this.scatterPlot.addData(
            this.generateFakePointData(
                function(x) {
                    return Math.pow(x / 1000, 2) * 1000;
                },
                this.numberOfDataPoints, "blue"
            )
        );
}

CustomGameLoop.prototype.setUpAccelleratedScatterPlot = function() {
    this.scatterPlot = 
            new AccelleratedScatterPlot(AccelleratedScatterPlot.bufferSize);
    this.scatterPlot.addData(
            this.generateFakePointData(
                function(x) {
                    return Math.sin(x / 1000 * Math.PI) * 0.7 * 1000;
                },
                this.numberOfDataPoints, "red"
            )
        );
    this.scatterPlot.addData(
            this.generateFakePointData(
                function(x) {
                    return Math.pow(x / 1000, 2) * 1000;
                },
                this.numberOfDataPoints, "blue"
            )
        );
}

CustomGameLoop.prototype.setUpQuadScatterPlot = function() {
    this.scatterPlot = new QuadScatterPlot(4);
    this.scatterPlot.addData(
            this.generateFakePointData(
                function(x) {
                    return Math.sin(x / 1000 * Math.PI) * 0.7 * 1000;
                },
                this.numberOfDataPoints, "red"
            )
        );
    this.scatterPlot.addData(
            this.generateFakePointData(
                function(x) {
                    return Math.pow(x / 1000, 2) * 1000;
                },
                this.numberOfDataPoints, "blue"
            )
        );
    this.scatterPlot.preRender();
}

CustomGameLoop.prototype.onPointerEnter = function(id, position) {
    this.inputManager.onPointerEnter(id, position);
}

CustomGameLoop.prototype.onPointerMove = function(id, position) {
    this.inputManager.onPointerMove(id, position);
}

CustomGameLoop.prototype.onPointerActivate = function(id, position) {
    this.inputManager.onPointerActivate(id, position);
}

CustomGameLoop.prototype.onPointerDeactivate = function(id, position) {
    this.inputManager.onPointerDeactivate(id, position);
}

CustomGameLoop.prototype.onPointerLeave = function(id, position) {
    this.inputManager.onPointerLeave(id, position);
}

CustomGameLoop.prototype.draw = function(g) {
    g.save();
    if (CustomGameLoop.isDebugZoomEnabled) {
        g.scale(0.5, 0.5);
        g.translate(500, 500);
    }
    g.fillStyle = "darkGray";
    g.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.scatterPlot.draw(g);
    if (CustomGameLoop.isVisualDebugEnabled) {
        this.inputManager.drawPointerDebugOverlay(g);
        g.lineWidth = 10;
        g.strokeStyle = "red";
        g.beginPath()
        g.rect(0, 0, this.canvas.width, this.canvas.height);
        g.stroke();
    }
    g.restore();
}
// </editor-fold>

// <editor-fold desc="InputManager">
function InputManager() {
    this.pointers = { };
    this.pointerCount = 0;
    this.activePointerCount = 0;
    this.pivot = new Point(0, 0);
    this.previousPivot = new Point(0, 0);
    this.pivotRadius = 0;
    this.previousPivotRadius = 0;
    this.manipulationTarget = null;
}

InputManager.prototype.setManipulationTarget = function(target) {
    this.manipulationTarget = target;
}

InputManager.prototype.onPointerEnter = function(id, position) {
    this.addPointer(id, position);
    if (id != GameLoop.Settings.Input.MOUSE_ID) {
        if (this.hasPointer(GameLoop.Settings.Input.MOUSE_ID)) {
            this.onPointerLeave(GameLoop.Settings.Input.MOUSE_ID, 
                new Point(0, 0));
        }
    }
}

InputManager.prototype.onPointerMove = function(id, position) {
    if (this.hasPointer(id)) {
        this.movePointer(id, position);
    }
}

InputManager.prototype.onPointerActivate = function(id, position) {
    if (this.hasPointer(id)) {
        this.pointers[id].activate();
        this.activePointerCount++;
        this.updatePivot();
        this.updatePivot();
    }
}

InputManager.prototype.onPointerDeactivate = function(id, position) {
    if (this.hasPointer(id)) {
        this.pointers[id].deactivate();
        this.activePointerCount--;
    }
}

InputManager.prototype.onPointerLeave = function(id, position) {
    if (this.hasPointer(id)) {
        this.removePointer(id, position);
    }
}

InputManager.prototype.hasPointer = function(id) {
    return typeof this.pointers[id] != 'undefined';
}

InputManager.prototype.getPointers = function() {
    var returnArray = [];
    for (var id in this.pointers) {
        returnArray.push(this.pointers[id]);
    }
    return returnArray;
}

InputManager.prototype.addPointer = function(id, initialPosition) {
    this.pointers[id] = new Pointer(id, initialPosition);
    this.pointerCount++;
}

InputManager.prototype.movePointer = function(id, position) {
    this.pointers[id].move(position);
    if (this.pointers[id].getIsActive()) {
        this.updatePivot();
        this.onManipulation(this.pointers[id]);
    }
}

InputManager.prototype.removePointer = function(id, position) {
    delete this.pointers[id];
    this.pointerCount--;
    this.updatePivot();
}

InputManager.prototype.drawPointerDebugOverlay = function(g) {
    for (var id in this.pointers) {
        this.pointers[id].drawDebugOverlay(g);
    }
    if (this.activePointerCount > 0) {
        g.beginPath();
        g.strokeStyle = "green";
        g.lineWidth = 1;
        g.arc(this.pivot.getX(), this.pivot.getY(), 10, 0, Math.PI * 2);
        g.stroke();
        g.strokeStyle = "gray";
        g.beginPath();
        g.arc(this.pivot.getX(), this.pivot.getY(), this.pivotRadius, 
            0, Math.PI * 2);
        g.stroke();
    }
    g.fillStyle = "gray";
    g.font = "10px Arial";
    g.fillText("Pointers: " + this.pointerCount + " (Active: " + 
            this.activePointerCount + ")", 5, 12);
}

InputManager.prototype.updatePivot = function() {
    this.previousPivot = this.pivot.clone();
    this.previousPivotRadius = this.pivotRadius;
    if (this.activePointerCount > 0) {
        var xAverage = 0;
        var yAverage = 0;
        for (var id in this.pointers) {
            if (this.pointers[id].getIsActive()) {
                xAverage += this.pointers[id].getPosition().getX();
                yAverage += this.pointers[id].getPosition().getY();
            }
        }
        xAverage /= this.activePointerCount;
        yAverage /= this.activePointerCount;
        this.pivot = new Point(xAverage, yAverage);
        var distanceAverage = 0;
        for (var id in this.pointers) {
            if (this.pointers[id].getIsActive()) {
                distanceAverage += this.pointers[id].getPosition()
                    .subtract(this.pivot).getLength();
            }
        }
        distanceAverage /= this.activePointerCount;
        this.pivotRadius = distanceAverage;
    } else {
        this.pivot = new Point(0, 0);
    }
}

InputManager.prototype.onManipulation = function(movedPointer) {
    if (this.manipulationTarget != null) {
        // translation
        var xTranslation = this.pivot.getX() - this.previousPivot.getX();
        var yTranslation = this.pivot.getY() - this.previousPivot.getY();
        this.manipulationTarget.translate(xTranslation, yTranslation);
        if (this.previousPivotRadius > 0) {
            // scale
            var scale = this.pivotRadius / this.previousPivotRadius;
            this.manipulationTarget.scaleAt(scale, this.pivot);
        }  
    }
}
// </editor-fold>

// <editor-fold desc="Pointer">
function Pointer(id, initialPosition) {
    this.id = id;
    this.position = initialPosition.clone();
    this.previousPosition = initialPosition.clone();
    this.isActive = false;
}

Pointer.prototype.move = function(position) {
    this.previousPosition = this.position.clone();
    this.position.setX(position.getX());
    this.position.setY(position.getY());
}

Pointer.prototype.getPosition = function() {
    return this.position.clone();
}

Pointer.prototype.getPreviousPosition = function() {
    return this.previousPosition.clone();
}

Pointer.prototype.getIsActive = function() {
    return this.isActive;
}

Pointer.prototype.drawDebugOverlay = function(g) {
    g.strokeStyle = "black";
    g.fillStyle = "black";
    g.font = "10px Arial"
    g.lineWidth = this.getIsActive() ? 3 : 1;
    g.globalAlpha = this.getIsActive() ? 1 : 0.5;
    var position = this.getPosition();
    g.beginPath();
    g.rect(position.getX() - 20, position.getY() - 20, 40, 40);
    g.stroke();
    g.fillText(this.id, position.getX() - 20, position.getY() - 20 - 3);
    g.globalAlpha = 1.0;
}

Pointer.prototype.activate = function() {
    this.isActive = true;
}

Pointer.prototype.deactivate = function() {
    this.isActive = false;
}
// </editor-fold>

function initialize() {
    var customGameLoop = new CustomGameLoop();
    customGameLoop.initialize(document.getElementById("canvas"));
}

window.onload = initialize;