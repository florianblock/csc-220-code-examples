// <editor-fold desc="ExtendedGameLoop">
function ExtendedGameLoop() {
    
}

ExtendedGameLoop.enableHighDPI = false;

ExtendedGameLoop.prototype = new GameLoop();

ExtendedGameLoop.prototype.initialize = function(canvas) {
    GameLoop.prototype.initialize.call(this, canvas);
    this.height = 0;
    this.width = 0;
    this.devicePixelRatio = window.devicePixelRatio || 1,
    this.backingStoreRatio = this.g.webkitBackingStorePixelRatio ||
                            this.g.mozBackingStorePixelRatio ||
                            this.g.msBackingStorePixelRatio ||
                            this.g.oBackingStorePixelRatio ||
                            this.g.backingStorePixelRatio || 1,

    this.ratio = this.devicePixelRatio / this.backingStoreRatio;
    this.fillWindow();
    var _this = this;
    window.addEventListener('resize', 
            function() {
                _this.onWindowResize();
            }, false);
}

ExtendedGameLoop.prototype.onWindowResize = function() {
    this.fillWindow();
}

ExtendedGameLoop.prototype.fillWindow = function() {
    if (ExtendedGameLoop.enableHighDPI) {
        this.canvas.width = window.innerWidth * this.ratio;
        this.canvas.height = window.innerHeight * this.ratio;
        this.canvas.style.width = window.innerWidth + "px";
        this.canvas.style.height = window.innerHeight + "px";
        this.width = window.innerWidth;
        this.height = window.innerHeight;
    } else {
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.width = this.canvas.width;
        this.height = this.canvas.height;
    }
}

ExtendedGameLoop.prototype.clear = function(g) {
    GameLoop.prototype.clear.call(this, g);
    if (ExtendedGameLoop.enableHighDPI) {
        g.resetTransform();
        g.scale(this.ratio, this.ratio);
    }
}

ExtendedGameLoop.prototype.draw = function(g) {
    var markerSize = 50;
    g.fillStyle = "#CCCCCC";
    g.fillRect(0, 0, this.width, this.height);
    g.fillStyle = "black";
    g.font = "10px Arial";
    g.fillText("canvas.width = " + this.canvas.width + "px, " +
            "canvas.height = " + this.canvas.height + "px",
            5, 20);
    g.fillText("width = " + this.width + "px, " +
            "height = " + this.height + "px",
            5, 40);
    g.fillText("backingStorePixelRatio = " + 
            this.backingStoreRatio + "px, " +
            "devicePixelRatio = " + this.devicePixelRatio + "px",
            5, 60);
    g.fillText("ratio = " + 
            this.ratio,
            5, 80);
    g.fillStyle = "red";
    g.beginPath();
    g.arc(this.width / 2, this.height / 2, 100, 0, Math.PI * 2);
    g.fill();
    g.fillRect(this.width - markerSize, this.height - markerSize,
            markerSize, markerSize);
}
// </editor-fold>

function initialize() {
    var extendedGameLoop = new ExtendedGameLoop();
    extendedGameLoop.initialize(document.getElementById("canvas"));
}

window.onload = initialize;