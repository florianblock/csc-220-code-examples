function Visual() {
    this.position = new Point(0, 0);
}

Visual.prototype.setPosition = function(p) {
    this.position.setX(p.x);
    this.position.setY(p.y);
}

Visual.prototype.getPosition = function(p) {
    return this.position.clone();
}

Visual.prototype.draw = function(g) {
    
}