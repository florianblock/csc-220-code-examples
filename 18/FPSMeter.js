function FPSMeter() {
    this.frameDurations = [];
    this.lastFrameTime = null;
}

FPSMeter.prototype.registerFrame = function() {
    var currentTime = new Date().getTime();
    if (this.lastFrameTime != null) {
        var frameDuration = currentTime - this.lastFrameTime;
        this.frameDurations.push(frameDuration);
        if (this.frameDurations.length > 60) {
            this.frameDurations.shift();
        }
    }
    this.lastFrameTime = currentTime;
}

FPSMeter.prototype.draw = function(g) {
    g.font = "20px Arial";
    var fpsText = this.getAverageFPS() + " FPS";
    var width = g.measureText(fpsText).width;
    g.fillStyle = "#303030";
    g.fillRect(0, 0, width + 10, 25);
    g.fillStyle = "red";
    g.fillText(fpsText, 5, 20);
}

FPSMeter.prototype.getAverageFPS = function() {
    var averageFrameDuration = 0;
    for (var i = 0; i < this.frameDurations.length; i++) {
        averageFrameDuration += this.frameDurations[i];
    }
    averageFrameDuration /= this.frameDurations.length;
    return Math.round(1000 / averageFrameDuration); // * 10 / 10
}