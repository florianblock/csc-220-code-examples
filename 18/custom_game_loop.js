// <editor-fold desc="CustomGameLoop">
function CustomGameLoop() {
    
}

CustomGameLoop.prototype = new GameLoop();

CustomGameLoop.prototype.initialize = function(canvas) {
    GameLoop.prototype.initialize.call(this, canvas);
    this.initializeUI();
    this.fpsMeter = new FPSMeter();
    this.canvas.width = 1000;
    this.canvas.height = 500;
    this.elementManager = new ElementManager();
    this.elementManager.addElement(
            new StarField(
                this.canvas.width, this.canvas.height, StarField.baseStarCount / 200, 3, 1));
    this.elementManager.addElement(
            new StarField(
                this.canvas.width, this.canvas.height, StarField.baseStarCount / 100, 1, 0.5));
    this.elementManager.addElement(
            new StarField(
                this.canvas.width, this.canvas.height, StarField.baseStarCount / 10, 0.5, 0.25));
    this.elementManager.addElement(
            new StarField(
                this.canvas.width, this.canvas.height, StarField.baseStarCount, 0.2, 0.125));
    this.elementManager.addElement(new Spaceship());
    // add initialization here
    this.previousPositions = {};
}

CustomGameLoop.prototype.initializeUI = function() {
    this.plotType = "modeDirect";
    
    var starCount = this.getParameterByName("baseStarCount");
    var isBufferEnabled = this.getParameterByName("useBackBuffer");
    
    if (starCount != "") {
        StarField.baseStarCount = Number(starCount);
    }
    if (isBufferEnabled == "1") {
        StarField.isBufferEnabled = true;
    } else {
        StarField.isBufferEnabled = false;
    }
    
    document.getElementById("baseStarCount").value = StarField.baseStarCount;
    document.getElementById("useBackBuffer").checked = 
            StarField.isBufferEnabled;
}

CustomGameLoop.prototype.getParameterByName = function(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : 
            decodeURIComponent(results[1].replace(/\+/g, " ")); 
}

CustomGameLoop.prototype.draw = function(g) {
    this.fpsMeter.registerFrame();
    g.fillStyle = "black";
    g.fillRect(0, 0, this.canvas.width, this.canvas.height);
    // add drawing here
    this.elementManager.draw(g);
    this.fpsMeter.draw(g);
}

CustomGameLoop.prototype.onPointerMove = function(id, position) {
    if (typeof this.previousPositions[id] === 'undefined') {
    } else {
        var delta = position.subtract(this.previousPositions[id]);
        this.elementManager.scrollAllStarFields(delta.getY());
    }
    this.previousPositions[id] = position;
}

CustomGameLoop.prototype.onPointerLeave = function(id, position) {
    delete this.previousPositions[id];
}
// </editor-fold>

function initialize() {
    var customGameLoop = new CustomGameLoop();
    customGameLoop.initialize(document.getElementById("canvas"));
}

window.onload = initialize;