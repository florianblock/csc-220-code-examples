function Star(position, radius) {
    Visual.call(this);
    this.setPosition(position);
    this.radius = radius;
}

Star.prototype = new Visual();

Star.prototype.draw = function(g) {
    g.beginPath();
    g.arc(this.position.getX(), 
        this.position.getY(), this.radius, 0, 2 * Math.PI);
    g.fill();
}