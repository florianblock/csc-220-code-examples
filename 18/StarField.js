function StarField(
        screenWidth, screenHeight, starDensity, starRadius, speed) {
    Visual.call(this);
    this.screenWidth = screenWidth;
    this.screenHeight = screenHeight;
    this.starDensity = starDensity;
    this.starRadius = starRadius;
    this.speed = speed;
    this.stars = [];
    this.buffer = null;
    this.bufferG = null;
    for (var i = 0; i < this.starDensity; i++) {
        this.stars.push(
                new Star(this.generateRandomPosition(), this.starRadius));
    }
}

StarField.isBufferEnabled = true;
StarField.baseStarCount = 1000;

StarField.prototype = new Visual();

StarField.prototype.generateRandomPosition = function() {
    return new Point(Math.random() * this.screenWidth, 
        Math.random() * this.screenHeight);
}

StarField.prototype.draw = function(g) {
    this.position.setX(this.position.getX() - this.speed);
    if (this.position.getX() < -this.screenWidth) {
        this.position.setX(this.screenWidth + this.position.getX());
    }
    g.save();
    g.translate(this.position.getX(), this.position.getY());
    this.drawInternal(0, 0, g, StarField.isBufferEnabled);
    this.drawInternal(this.screenWidth, 0, g, StarField.isBufferEnabled);
    this.drawInternal(0, -this.screenHeight, g, StarField.isBufferEnabled);
    this.drawInternal(0, this.screenHeight, g, StarField.isBufferEnabled);
    this.drawInternal(this.screenWidth, -this.screenHeight, 
        g, StarField.isBufferEnabled);
    this.drawInternal(this.screenWidth, this.screenHeight, 
        g, StarField.isBufferEnabled);
    g.restore();
}

StarField.prototype.drawInternal = function(xOffset, yOffset, g, drawBuffer) {
    g.save();
    g.translate(xOffset, yOffset);
    if (!drawBuffer) {
        g.fillStyle = "white";
        for (var i = 0; i < this.stars.length; i++) {
            this.stars[i].draw(g);
        }
    } else {
        if (this.buffer == null) {
            this.instantiateBuffer();
        }
        g.drawImage(this.buffer, 0, 0);
    }
    g.restore();
}

StarField.prototype.instantiateBuffer = function() {
    this.buffer = document.createElement("canvas");
    this.buffer.width = this.screenWidth;
    this.buffer.height = this.screenHeight;
    this.bufferG = this.buffer.getContext("2d");
    this.drawInternal(0, 0, this.bufferG, false);
}

StarField.prototype.scrollVertically = function(delta) {
    var newY = this.position.getY() + delta * this.speed;
    if (newY < -this.screenHeight) {
        newY = -this.screenHeight - newY;
    }
    if (this.screenHeight < newY) {
        newY = newY - this.screenHeight;
    }
    this.position.setY(newY);
}