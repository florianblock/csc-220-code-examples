function InteractiveChart(parent)
{
    var template = SETTINGS.interactiveChartTemplate.cloneNode(true);
    this.chartAreaDiv = template.getElementsByClassName("chartArea")[0];
    var selectorElement = template.getElementsByClassName("dataSelector")[0];
    this.selector = new DataSelector(this, selectorElement);
    this.headlineDiv = template.getElementsByClassName("chartHeadline")[0];
    parent.appendChild(template);
    this.selectedDataSeries = DATA[0];
    this.showDataSeries();
}

InteractiveChart.prototype.showDataSeries = function()
{
    var chart = new BarChart(this.chartAreaDiv);
    chart.show(this.selectedDataSeries);
    this.headlineDiv.innerHTML = this.selectedDataSeries.getName();
}

InteractiveChart.prototype.onSelectionChanged = function(selectedElement)
{
    this.selectedDataSeries = selectedElement.getData();
    this.showDataSeries();
}

function DataSelector(interactiveChart, selectorElement) {
    this.interactiveChart = interactiveChart;
    this.items = [];
    selectorElement.innerHTML = "";
    for (var i = 0; i < DATA.length; i++)
    {
        this.items.push(new DataSelectorItem(DATA[i], selectorElement));
    }
    selectorElement.associatedSelector = this;
    selectorElement.onchange = function() {
        this.associatedSelector.onSelectionChanged(
                this.options[this.selectedIndex].associatedSelectorItem);
    }
}

DataSelector.prototype.onSelectionChanged = function(selectedElement)
{
    this.interactiveChart.onSelectionChanged(selectedElement);
}

function DataSelectorItem(dataSeries, selectorElement)
{
    this.dataSeries = dataSeries;
    var template = SETTINGS.selectorItemTemplate.cloneNode(true);
    template.innerHTML = dataSeries.getName();
    template.associatedSelectorItem = this;
    selectorElement.appendChild(template);
}

DataSelectorItem.prototype.getData = function()
{
    return this.dataSeries;
}

function BarChart(targetDiv) {
    this.targetDiv = targetDiv;
    this.bars = [];
}

BarChart.prototype.show = function(dataSeries) {
    this.targetDiv.innerHTML = "";
    for (var i = 0; i < dataSeries.getData().length; i++) {
        var dataPoint = dataSeries.getData()[i];
        var bar = new Bar(dataPoint);
        this.bars.push(bar);
        bar.show(this.targetDiv);
    }
}

function Bar(dataPoint) {
    this.data = dataPoint;
    this.barDiv = null;
    this.barValueDiv = null;
}

Bar.prototype.show = function(container) {
    var template = SETTINGS.barTemplate.cloneNode(true);
    this.barValueDiv = template.getElementsByClassName("barValue")[0];
    this.barValueDiv.innerHTML = this.data.getValue();
    this.barValueDiv.style.visibility = "hidden";
    this.barDiv = template.getElementsByClassName("bar")[0];
    this.barDiv.style.height = 
            (this.data.getValue() * SETTINGS.barScaleFactor) + "px";
    this.barDiv.associatedObject = this;
    this.barDiv.onmouseenter = function() {
        this.associatedObject.highlight();
    }
    this.barDiv.onmouseleave = function() {
        this.associatedObject.deHighlight();
    }
    var barLabelDiv = template.getElementsByClassName("barLabel")[0];
    barLabelDiv.innerHTML = this.data.getLabel();
    container.appendChild(template);
}

Bar.prototype.highlight = function()
{
    this.barDiv.className = "barHighlighted";
    this.barValueDiv.style.visibility = "visible";
}

Bar.prototype.deHighlight = function()
{
    this.barDiv.className = "bar";
    this.barValueDiv.style.visibility = "hidden";
}

function initializeTemplates()
{
    var interactiveChartTemplate = 
            document.getElementsByClassName("interactiveChartArea")[0]
            .cloneNode(true);
    interactiveChartTemplate.getElementsByClassName("chartArea")[0]
            .innerHTML = "";
    SETTINGS = {
        interactiveChartTemplate : interactiveChartTemplate,
        selectorItemTemplate : 
                document.getElementsByClassName("dataSelectorItem")[0]
                .cloneNode(true),
        barTemplate : document.getElementById("barTemplate").cloneNode(true),
        barScaleFactor : 1.5
    }
}

var SETTINGS = null;

function initialize() {
    initializeTemplates();
    document.body.innerHTML = "";
    var interactiveChart = new InteractiveChart(document.body);
}

window.onload = initialize; // Thanks, Giovanna!
